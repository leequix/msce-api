const bunyan = require('bunyan');

const config = require('./config');

const logger = name => bunyan.createLogger({
  name,
  level: config.get('log:level'),
});

module.exports = logger;
