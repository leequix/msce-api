const thinky = require('thinky');

const config = require('./config');

module.exports = thinky(config.get('rethinkdb'));
