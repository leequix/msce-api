const nconf = require('nconf');

const config = nconf.env({
  lowerCase: true,
  parseValues: true,
  separator: '_',
});

module.exports = config.defaults({
  http: {
    host: '127.0.0.1',
    port: 80,
  },
  log: {
    level: 'info',
  },
  rethinkdb: {
    host: '127.0.0.1',
    port: 28015,
    db: 'test',
  },
});
