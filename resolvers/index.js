const packageJson = require('../package.json');

module.exports = {
  Query: {
    version: () => packageJson.version,
  },
};
