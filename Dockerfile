FROM node:10.15.0-alpine

WORKDIR /var/www/msce-api

COPY . .

RUN npm install

EXPOSE 80

ENTRYPOINT node .
