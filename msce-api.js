const { ApolloServer } = require('apollo-server');

const { APP_LOGGER } = require('./constants/loggers');

const config = require('./utils/config');
const logger = require('./utils/logger');
const thinky = require('./utils/thinky');

const typeDefs = require('./schema');
const resolvers = require('./resolvers');


const log = logger(APP_LOGGER);
const httpConfig = config.get('http');

const server = new ApolloServer({ typeDefs, resolvers });

thinky.dbReady().then(() => {
  log.info('Connected to RethinkDB');
});

server.listen(httpConfig).then(({ url }) => {
  log.info('API is started', { url });
});
