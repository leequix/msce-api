const thinky = require('../utils/thinky');

const {
  type,
  r,
} = thinky;

const Group = thinky.createModel('groups', {
  id: type.string.uuid,
  number: type.string.min(2),
  lastUpdate: type.date.default(r.now()),
});

module.exports = Group;
